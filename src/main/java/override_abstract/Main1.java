package override_abstract;

public class Main1 {
    public static void main(String[] args) {
        Circle circle = new Circle("red", true, 3);
        System.out.printf("Diện tích: %.2f \n", circle.getArea());
        System.out.printf("Chu vi: %.2f \n", circle.getPerimeter());
        System.out.println(circle);

        Rectangle rectangle = new Rectangle("blue", false, 4, 5);
        System.out.println("Diện tích: " + rectangle.getArea());
        System.out.println("Chu vi: " + rectangle.getPerimeter());
        System.out.println(rectangle);

        Square square = new Square();
        square.setSide(5.0);
        System.out.println(square);
        System.out.printf("Chu vi %.2f cm, dien tich %.2f cm2 \n", square.getPerimeter(), square.getArea());
    }
}
