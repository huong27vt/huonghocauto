package tonghopoop1;

import java.sql.Array;

public class XyLyVanBan {
    public int demSoTu(VanBan vanBan) {
        String mangTu[] = vanBan.str.split(" ");
        int soTu = mangTu.length;
        return soTu;
    }

    public VanBan chuanHoaVanBan(VanBan vanBan) {
        String newStr = vanBan.str.trim().replaceAll("\\s+", " ");
        return new VanBan(newStr);
    }

    public VanBan upperCaseVB(VanBan vanBan) {
        String newStr = vanBan.str.toUpperCase();
        return new VanBan(newStr);
    }

    public VanBan lowerCaseVB(VanBan vanBan) {
        String newStr = vanBan.str.toLowerCase();
        return new VanBan(newStr);
    }

    public VanBan vietHoaChuDau(VanBan vanBan) {
        String[] mangTu = vanBan.str.split(" ");
        String chuoiVietHoaChuDau = "";
        for (int i = 0; i < mangTu.length; i++) {
            //System.out.println(""+mangTu[i]);
            String first = mangTu[i].substring(0, 1).toUpperCase();
            String remain = mangTu[i].substring(1, mangTu[i].length()).toLowerCase();
            String newStr = first + remain;
            chuoiVietHoaChuDau
                    = chuoiVietHoaChuDau + " " + newStr;
        }
        return new VanBan(chuoiVietHoaChuDau);
    }
}
