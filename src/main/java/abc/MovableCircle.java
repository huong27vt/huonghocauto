package abc;

public class MovableCircle implements Movable{
    private int radius;
    private MovablePoint center;

    public MovableCircle(int x, int y, int xSpeed, int ySpeed, int radius) {
        this.radius = radius;
        this.center = new MovablePoint(x, y, xSpeed, ySpeed);
    }

    @Override
    public String toString() {
        return center.toString() + ",radius=" + radius;
    }

    @Override
    public void moveUp() {
        System.out.println("Di chuyển lên 1");
    }

    @Override
    public void moveDown() {
        System.out.println("Di chuyển xuống 2");
    }

    @Override
    public void moveLeft() {
        System.out.println("Di chuyển sang trái 3");
    }

    @Override
    public void moveRight() {
        System.out.println("Di chuyển sang phải 4");
    }
}
