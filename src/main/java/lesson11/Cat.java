package lesson11;

public class Cat extends Animal{
    public Cat(String name, String color, String size) {
        super(name, color, size);
    }

    @Override
    public void makeSound() {
        System.out.println("meo meo");
    }

    @Override
    public void move() {
        System.out.println("Di chuyển bằng 4 chân");
    }
}
