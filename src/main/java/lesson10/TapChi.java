package lesson10;

public class TapChi extends TaiLieu{
    private String no;

    public TapChi(String code, String nsx, int noPublish, String no) {
        super(code, nsx, noPublish);
        this.no = no;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }


}
