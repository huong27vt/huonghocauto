package abc;

public class Main {
    public static void main(String[] args) {
        MovableCircle movableCircle = new MovableCircle(1,2,3,4,5);
        System.out.println(movableCircle.toString());
        movableCircle.moveUp();
        movableCircle.moveDown();
        movableCircle.moveLeft();
        movableCircle.moveRight();

        MovablePoint movablePoint = new MovablePoint(6,7,8, 9);
        System.out.println(movablePoint.toString());
        movablePoint.moveUp();
        movablePoint.moveDown();
        movablePoint.moveLeft();
        movablePoint.moveRight();
    }
}
