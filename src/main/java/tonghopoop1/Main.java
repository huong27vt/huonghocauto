package tonghopoop1;

public class Main {
    public static void main(String[] args) {
        XyLyVanBan xuLyVanBan = new XyLyVanBan();
        VanBan vanBan = new VanBan("  Những   điểm cần GHI nHỚ  về ArrayList  ");
        System.out.println("Chuỗi ban đầu: " + vanBan);

        System.out.println("Số từ là: " + xuLyVanBan.demSoTu(vanBan));
        System.out.println("Chuỗi sau khi chuẩn hóa là: " + xuLyVanBan.chuanHoaVanBan(vanBan));
        System.out.println("Chuỗi sau khi viết hoa là: " + xuLyVanBan.upperCaseVB(xuLyVanBan.chuanHoaVanBan(vanBan)));
        System.out.println("Chuỗi sau khi viết thường là: " + xuLyVanBan.lowerCaseVB(xuLyVanBan.chuanHoaVanBan(vanBan)));
        System.out.println("Chuỗi viết hoa chữ cái đầu là: " + xuLyVanBan.vietHoaChuDau(xuLyVanBan.chuanHoaVanBan(vanBan)));
    }
}
