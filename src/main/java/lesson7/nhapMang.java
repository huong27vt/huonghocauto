package lesson7;

import java.util.Arrays;
import java.util.Scanner;

public class nhapMang {
    public static int[] nhapMang(int[] arr){
        Scanner scanner = new Scanner(System.in);
        int n;
        do {
            System.out.println("Nhập vào số phần tử của mảng: ");
            n = scanner.nextInt();
        } while (n < 0);

        arr = new int[n];
        System.out.println("Nhập các phần tử của mảng:");
        for (int i = 0; i < n; i++) {
            System.out.printf("Nhập phần tử thứ " + i + ": ");
            arr[i] = scanner.nextInt();
        }
        scanner.close();
        System.out.println("Các phần tử của mảng: " + Arrays.toString(arr));
        return arr;
    }
}
