package baitapinterface;

public class HinhChuNhat extends HinhBinhHanh{

    public HinhChuNhat(double canhDay, double canhKeDay) {
        super(canhDay, canhKeDay);
    }
    public double tinhDienTich() {
        return canhDay*canhKeDay;
    }

    @Override
    public String toString() {
        return "HinhChuNhat{" +
                "canhDay=" + canhDay +
                ", canhKeDay=" + canhKeDay +
                '}';
    }
}
