package generic.bai1;

public interface IHinhHoc <T>{
    public double tinhChuVi();
    public double tinhDienTich();
}
