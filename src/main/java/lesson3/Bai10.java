package lesson3;

import java.util.Arrays;

public class Bai10 {
    public static void main(String[] args) {
        int[] myNum = {1,2,3,4,5,6};
//        System.out.println("Array reverse is: ");
//        for(int i = myNum.length - 1; i >= 0; i--){
//            System.out.print(myNum[i] + " ");
//        }

        int i = 0 ;
        while (i < myNum.length/2){
            int temp = myNum[i];
            myNum[i] = myNum[myNum.length-i-1];
            myNum[myNum.length-i-1] = temp;
            ++i;
        }
        System.out.println(Arrays.toString(myNum));
    }
}
