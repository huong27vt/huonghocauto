package lesson3;

import java.util.Arrays;

public class Bai4 {
    public static void main(String[] args) {
        int[] myNum = {1,2,3,4,5,6};
        myNum[3] = 9;
        System.out.println("New array: " + Arrays.toString(myNum));
    }
}
