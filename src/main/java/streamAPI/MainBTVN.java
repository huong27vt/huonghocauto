package streamAPI;

import com.github.javafaker.Faker;

import java.util.*;
import java.util.stream.Collectors;

public class MainBTVN {
    public static void main(String[] args) {

        // Tạo 1000 đối tượng Human sử dụng Faker (fake tiếng việt)
        Faker faker = new Faker(new Locale("vi", "VN"));
        List<Human> humanList = new ArrayList<>();
        for(int i = 0; i < 1000; i++){
            Human human = new Human();
            human.setId(i+1);
            human.setFirstName(faker.name().firstName());
            human.setLastName(faker.name().lastName());
            human.setCity(faker.country().capital());
            human.setGender(faker.number().numberBetween(0,2));
            human.setAge(faker.number().numberBetween(10, 51));
            human.setSalary(faker.number().numberBetween(10, 3001));
            humanList.add(human);
            System.out.println(human);
        }

        // Có bao nhiêu nam trên 18 tuổi
        long soNamTren18 = humanList.stream().filter(i -> i.getAge() > 18).filter(i -> i.getGender() == 1).count();
        System.out.println("Số lượng nam trên 18 tuồi là: " + soNamTren18);


        // Lọc ra những người họ Nguyễn và sắp xếp theo thứ tự chữ cái
        System.out.println("Lọc ra những người họ Nguyễn và sắp xếp theo thứ tự chữ cái");
        List<Human> humanNguyen = new ArrayList<>();
        humanNguyen = humanList.stream().filter(i -> i.getLastName().equals("Nguyễn")).collect(Collectors.toList());
        if(humanNguyen.size() == 0){
            System.out.println("Không có người họ Nguyễn nào");
        }else{
            humanNguyen.sort(Comparator.comparing(Human::getFirstName));
            humanNguyen.forEach(t -> System.out.println(t));
        }

        // In ra danh sách những người độ tuổi từ 20-30
        System.out.println("In ra danh sách những người độ tuổi từ 20-30");
        List<Human> humanAge2030 = new ArrayList<>();
        humanAge2030 = humanList.stream().filter(i -> i.getAge() >= 20 && i.getAge() <= 30).collect(Collectors.toList());
        if(humanAge2030.size() == 0){
            System.out.println("Không có người thuộc độ tuổi 20-30");
        }else{
            humanAge2030.forEach(i -> System.out.println(i));
        }


        // Đếm số lượng thành phố có trong danh sách trên: Lấy ra ds thành phố -> đếm
        List<String> listThanhPho = humanList.stream().map(i -> i.getCity()).distinct().collect(Collectors.toList());
        System.out.println("Tổng thành phố: " + listThanhPho.size());

        // Lấy ra 10 người đầu tiên trong danh sách
        System.out.println("Lấy ra 10 người đầu tiên trong danh sách");
        List<Human> limit = humanList.stream().limit(10).collect(Collectors.toList());
        limit.forEach(t -> System.out.println(t));

        // Tính độ tuổi trung bình
        double tuoiTrungBinh = humanList.stream().collect(Collectors.averagingDouble(h -> h.getAge()));
        System.out.println(tuoiTrungBinh);

        // Tính mức lương trung bình của nam ở độ tuổi từ 20-40
        double luongTrungBinh = humanList.stream()
                .filter(h -> h.getGender() == 1)
                .filter(h -> h.getAge() >= 20 && h.getAge() <= 40)
                .collect(Collectors.averagingDouble(h -> h.getSalary()));
        System.out.println("Lương trung bình là: " + luongTrungBinh);

        // Sắp xếp danh sách theo độ tuổi

        Map<String, List<Human>> listHumanByCity;
    }
}
