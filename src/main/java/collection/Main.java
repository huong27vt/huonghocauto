package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Nguoi> p1 = new ArrayList<>();
        p1.add(new Nguoi(1, "Nguyen Van A", 20));
        p1.add(new Nguoi(2, "Do Van B", 21));
        p1.add(new Nguoi(2, "Phan Van C", 22));
        Collections.sort(p1, new Comparator<Nguoi>() {
            @Override
            public int compare(Nguoi o1, Nguoi o2) {
                if(o1.hoTen.charAt(0) < o2.hoTen.charAt(0)){
                    return -1;
                } else if(o1.hoTen.charAt(0) == o2.hoTen.charAt(0)){
                    return 0;
                }
                return 1;
            }


        });
        System.out.println(p1);

    }
}
