package baitapinterface;

public class Main1 {
    public static void main(String[] args) {
        Circle circle = new Circle(4);
        System.out.printf("Diện tích là: %.2f \n", circle.getArea());
        System.out.printf("Chu vi là: %.2f \n", circle.getPerimeter());
        System.out.println(circle);

        ResizableCircle resizableCircle = new ResizableCircle(10);
        resizableCircle.resize(2);
        System.out.println(resizableCircle);

    }
}
