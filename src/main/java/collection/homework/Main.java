package collection.homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    static Scanner input = new Scanner(System.in);
    static List<SinhVien> sinhViens = new ArrayList<>();
    static List<MonHoc> monHocs = new ArrayList<>();
    public static void main(String[] args) {
        System.out.println("MENU: ");
        System.out.println("1. Danh sách sinh viên");
        System.out.println("2. Nhập thông tin sinh viên mới");
        System.out.println("3. Tìm kiếm sinh viên theo ID");
        System.out.println("4. Danh sách môn học");
        System.out.println("5. Nhập thông tin môn học mới");
        System.out.println("6. Nhập điểm môn học cho sinh viên");
        System.out.println("7. Cập nhật điểm cho sinh viên");
        System.out.println("8. Xem điểm sinh viên theo mã ID");
        System.out.println("9. In ra danh sách sinh viên theo các mã môn học");
        System.out.println("10. In ra sinh viên có điểm trung bình tất cả các môn cao nhất");
        System.out.println("Mời bạn chọn chương trình: ");

        int select = 0;
        try {
            select = Integer.parseInt(input.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Bạn phải nhập số thuộc khoảng 1-10");
        }

        // Khởi tạo mảng sinh viên ban đầu
        sinhViens.add(new SinhVien("SV01", "Hương", "02/07/1996", "Hải Dương"));
        sinhViens.add(new SinhVien("SV02", "Hoa", "02/08/1997", "Nghệ An"));
        // Khởi tạo mảng môn học ban đầu
        monHocs.add(new MonHoc("MH01", "Toán"));
        monHocs.add(new MonHoc("MH02", "Hóa"));


        switch (select){
            case 1:
                inDsSv(sinhViens);
                break;
            case 2:
                nhapSV();
                inDsSv(sinhViens);
                break;
            case 3:
                timKiem(sinhViens);
                break;
            case 4:
                inDsMH(monHocs);
                break;
            case 5:
                nhapMH();
                inDsMH(monHocs);
                break;
        }

    }


    public static void inDsSv(List<SinhVien> sinhViens){
        if(sinhViens.size() >= 1){
            System.out.println("Danh sách sinh viên là: ");
            for(int i = 0; i < sinhViens.size(); i++){
                System.out.println(sinhViens.get(i));
            }
        }else {
            System.out.println("Chưa có sinh viên nào");
        }
    }

    public static void nhapSV(){
        System.out.println("Nhập thông tin sinh viên mới");
        System.out.println("Nhập mã: ");
        String maSV = input.nextLine();
        System.out.println("Nhập tên: ");
        String ten = input.nextLine();
        System.out.println("Nhập ngày sinh: ");
        String ngaySinh = input.nextLine();
        System.out.println("Nhập quê quán: ");
        String queQuan = input.nextLine();
        sinhViens.add(new SinhVien(maSV, ten, ngaySinh, queQuan));
    }

    public static void timKiem(List<SinhVien> sinhViens){
        System.out.println("Nhập mã sinh viên muốn tìm kiếm: ");
        String maSV = input.nextLine();
        boolean kq = false;
        for(int i = 0; i < sinhViens.size(); i++ ){
            if(maSV.equals(sinhViens.get(i).getMaSV())){
                System.out.println("Sinh viên tìm được là: ");
                System.out.println(sinhViens.get(i));
                kq = true;
            }else{
                kq = false;
            }
        }
        if(kq == false){
            System.out.println("Không tìm thấy sinh viên nào");
        }
    }

    public static void inDsMH(List<MonHoc> monHocs){
        if(monHocs.size() >= 1){
            System.out.println("Danh sách môn học là: ");
            for(int i = 0; i < monHocs.size(); i++){
                System.out.println(monHocs.get(i));
            }
        }else {
            System.out.println("Chưa có môn học nào");
        }
    }
    public static void nhapMH(){
        System.out.println("Nhập thông tin môn học mới");
        System.out.println("Nhập mã: ");
        String maMH = input.nextLine();
        System.out.println("Nhập tên: ");
        String ten = input.nextLine();
        monHocs.add(new MonHoc(maMH, ten));
    }

}
