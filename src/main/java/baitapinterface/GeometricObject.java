package baitapinterface;

public interface GeometricObject {
    public double getPerimeter();
    public double getArea();
}
