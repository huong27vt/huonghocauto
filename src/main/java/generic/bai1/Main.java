package generic.bai1;

public class Main {
    public static void main(String[] args) {
        Drawable<HinhVuong> hinhVuong = new Drawable<>();
        hinhVuong.draw(new HinhVuong(5));

        Drawable<HinhTron> hinhTron = new Drawable<>();
        hinhTron.draw(new HinhTron(4));
    }
}
