package lesson10;

import java.util.Scanner;

public class Sach extends  TaiLieu{
    private String author;
    private int noPage;
    private double price;

    public Sach(String code, String nsx, int noPublish, String author, int noPage, double price) {
        super(code, nsx, noPublish);
        this.author = author;
        this.noPage = noPage;
        this.price = price;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getNoPage() {
        return noPage;
    }

    public void setNoPage(int noPage) {
        this.noPage = noPage;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public void input() {
        Scanner scan = new Scanner(System.in);
        super.input();
        System.out.println("Nhập tên tác giả: ");
        author = scan.nextLine();
        System.out.println("Nhập số trang");
        noPage = scan.nextInt();
        System.out.println("Nhập giá: ");
        price = scan.nextInt();
    }
}
