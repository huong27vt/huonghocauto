package lesson10;

public class Bao extends TaiLieu{
    private String date;

    public Bao(String code, String nsx, int noPublish, String date) {
        super(code, nsx, noPublish);
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


}
