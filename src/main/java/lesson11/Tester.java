package lesson11;

public class Tester extends NhanVien{

    public Tester(String maNV, String hoTen, String gioiTinh, int namSinh) {
        super(maNV, hoTen, gioiTinh, namSinh);
    }

    @Override
    public void congViec() {
        System.out.println("Kiểm thử phần mềm");
    }
}
