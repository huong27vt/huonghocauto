package generic.bai1;

public class HinhTron implements IHinhHoc{
    double banKinh;

    public HinhTron() {
    }

    public HinhTron(double banKinh) {
        this.banKinh = banKinh;
    }

    @Override
    public double tinhChuVi() {
        return 2*banKinh*3.14;
    }

    @Override
    public double tinhDienTich() {
        return banKinh*banKinh*3.14;
    }

    @Override
    public String toString() {
        return "HinhTron[r = " + banKinh +
                ']';
    }
}
