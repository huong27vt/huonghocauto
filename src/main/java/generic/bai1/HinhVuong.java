package generic.bai1;

public class HinhVuong implements IHinhHoc{

    double canh;

    public HinhVuong() {
    }

    public HinhVuong(double canh) {
        this.canh = canh;
    }

    @Override
    public double tinhChuVi() {
        return 4*canh;
    }

    @Override
    public double tinhDienTich() {
        return canh * canh;
    }

    @Override
    public String toString() {
        return "HinhVuong[canh = " + canh +
                ']';
    }
}
