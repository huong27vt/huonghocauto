package generic;

public class Main1 {
    public static void main(String[] args) {
        Dictionary<String, String>[] tuDien = new Dictionary[100];
        tuDien[0] = new Dictionary("Hello", "Xin chào");

        for(Dictionary item: tuDien){
            if(item == null) break;
            System.out.println(item);
        }

        Dictionary<String, String[]>  t = new Dictionary<>();
        t.setT("Hello");
        t.setV(new String[]{"chòa", "xin chào"});
        System.out.println(t);
//        Dictionary<String, String> dictionary = new Dictionary<>("Hello", "Xin chào");
//        System.out.println(dictionary);
//        Dictionary<Integer, String> dictionary1 = new Dictionary<>(1, "192.168.1.10");
//        System.out.println(dictionary1);
//        Dictionary<String, String> dictionary2 = new Dictionary<>("Hương", "0348951027");
//        System.out.println(dictionary2);
    }
}
