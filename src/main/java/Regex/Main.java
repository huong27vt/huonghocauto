package Regex;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập SĐT: ");
        String SĐT = scanner.nextLine();
        if(SĐT.matches("^(\\(\\+84\\)|\\+84|0)(\\s|\\.|\\-)?(3[2-9]|86|9[678])(\\d?)(\\s|\\.|\\-)?(\\d{3,4})(\\s|\\.|-)?(\\d{3})")){
            System.out.println(SĐT + " là số Viettel");
        }else{
            System.out.println(SĐT + " không là số Viettel");
        }
    }
}
