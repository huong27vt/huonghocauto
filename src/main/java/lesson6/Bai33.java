package lesson6;

import java.util.Scanner;

public class Bai33 {
    public static void main(String[] args) {
        int i, n;
        float s;
        Scanner input = new Scanner(System.in);
        do
        {
            System.out.println("Nhập n (n >= 1): ");
            n = input.nextInt();
            if(n < 1)
            {
                System.out.println("n phải >= 1, mới bạn nhập lại!");
            }
        }while(n < 1);
        input.close();
        s = (float) Math.sqrt(2);
        for(i = 2; i <= n; i++)
        {
            s = (float) Math.sqrt(2 + s);
        }

        System.out.println("Tổng là: " + s);
    }
}

