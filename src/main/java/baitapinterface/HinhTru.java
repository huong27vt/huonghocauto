package baitapinterface;

public class HinhTru extends HinhTron implements HinhHoc3D{
    protected double chieuCao;

    public HinhTru(double radius, double chieuCao) {
        super(radius);
        this.chieuCao = chieuCao;
    }

    public double getChieuCao() {
        return chieuCao;
    }

    public void setChieuCao(double chieuCao) {
        this.chieuCao = chieuCao;
    }

    @Override
    public String toString() {
        return "HinhTru{" +
                super.toString() +
                ", chieuCao=" + chieuCao +
                '}';
    }

    @Override
    public double tinhTheTich() {
        return Math.PI*radius*radius*chieuCao;
    }
}
