package generic;

public class Fruits {
    String name;

    public Fruits() {
    }

    public Fruits(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Fruits{" +
                "name='" + name + '\'' +
                '}';
    }
}
