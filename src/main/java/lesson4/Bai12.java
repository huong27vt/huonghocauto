package lesson4;

import java.util.Scanner;

public class Bai12 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int numberOfDayInMonth = 0;
        System.out.print("Nhập năm: ");
        int year = input.nextInt();
        System.out.print("Nhập tháng: ");
        int month = input.nextInt();
        if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12){
            numberOfDayInMonth = 31;
        }else if(month == 4 || month == 6 || month == 9 || month == 11){
            numberOfDayInMonth = 30;
        }else if(month == 2){
            if((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))){
                numberOfDayInMonth = 29;
            }else{
                numberOfDayInMonth = 28;
            }
        }else {
            System.out.println("Tháng không hợp lệ");
        }
        System.out.println("Tháng " + month + " " + year + " có " + numberOfDayInMonth + " ngày");
    }
}
