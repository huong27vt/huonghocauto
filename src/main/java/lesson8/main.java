package lesson8;

public class main {
    public static void main(String[] args) {
        HCN hcn1 = new HCN();
        hcn1.chieuDai = 3;
        hcn1.chieuRong = 5;
        System.out.println(hcn1.tinhChuVi());
        System.out.println(hcn1.tinhDienTich());

        HCN hcn2 = new HCN(4.5, 5.6);
        System.out.println(hcn2.tinhChuVi());
        System.out.println(hcn2.tinhDienTich());
    }
}
