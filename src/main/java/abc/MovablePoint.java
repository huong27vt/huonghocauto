package abc;

public class MovablePoint implements Movable{
    int x;
    int y;
    int xSpeed;
    int ySpeed;

    public MovablePoint(int x, int y, int xSpeed, int ySpeed) {
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + "),speed=(" + x + "," + y + ")";

    }

    @Override
    public void moveUp() {
        System.out.println("Di chuyển lên");
    }

    @Override
    public void moveDown() {
        System.out.println("Di chuyển xuống");
    }

    @Override
    public void moveLeft() {
        System.out.println("Di chuyển sang trái");
    }

    @Override
    public void moveRight() {
        System.out.println("Di chuyển sang phải");
    }
}
