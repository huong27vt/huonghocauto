package baitapinterface;

public class HinhTron implements HinhHoc2D{
    protected double radius;

    public HinhTron(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "HinhTron{" +
                "radius=" + radius +
                '}';
    }

    @Override
    public double tinhChuVi() {
        return 2*radius*Math.PI;
    }

    @Override
    public double tinhDienTich() {
        return radius*radius*Math.PI;
    }
}
