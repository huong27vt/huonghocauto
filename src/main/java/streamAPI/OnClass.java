package streamAPI;

import java.util.Arrays;
import java.util.function.IntConsumer;

public class OnClass {
        public static void main(String[] args) {
            // Danh sách các số nguyên
            int[] daySo = {1, 2, 5, -5, 0, 7, 8, 10, 9, 20, -4, 6, 7, 30, 40};
            // In ra danh sách các số có trong dãy
            Arrays.stream(daySo).forEach(new IntConsumer() {
                @Override
                public void accept(int value) {
                    System.out.print(value + "\t");
                }
            });
            System.out.println();
            // In ra các số dương (lớn hơn 0).
            Arrays.stream(daySo).filter(i -> i > 0).forEach(i -> System.out.print(i + "\t"));
            System.out.println();
            // Đếm các số âm trong dãy
            // C1: thông thường
            int count = 0;
            for (int i : daySo) {
                if (i < 0) count++;
            }
            System.out.printf("Có %d số âm \n", count);
            // C2: sử dungk stream api: dãy -> stream -> lọc (< 0) -> đếm
            long count1 = Arrays.stream(daySo).filter(i -> i < 0).count();
            System.out.printf("Có %d số âm \n", count1);

            Arrays.stream(daySo).filter(i -> i > 0).filter(i -> i % 2 == 0).forEach(i -> System.out.print(i + "\t"));
            System.out.println();
        }
}
