package Buoi9;


import java.util.Scanner;

public class Main {
    static Scanner input = new Scanner(System.in);
    public static void main(String[] args) {
        PhanSo mangPhanSo[];
        PhanSo tong, hieu, tich, thuong;
        TinhToan tinhToan = new TinhToan();

        System.out.println("------MENU------");
        System.out.println("1. Tính tổng 3 phân số");
        System.out.println("2. Tính hiệu 3 phân số");
        System.out.println("3. Tính tích 2 phân số");
        System.out.println("4. Tính thương 2 phân số");
        System.out.println("5. Lịch sử 5 phép tính gần nhất");
        System.out.println("Mời bạn chọn chương trình");
        int select = 0;
        try {
            select = Integer.parseInt(input.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Bạn phải nhập số thuộc khoảng 1-5");
        }
        switch (select) {
            case 1:
                mangPhanSo = nhapMangPS(3);
                tong = tinhToan.cong(mangPhanSo);
                System.out.println("Tổng các phân số là: " + tong.tuSo + "/" + tong.mauSo);
                break;
            case 2:
                mangPhanSo = nhapMangPS(3);
                hieu = tinhToan.tru(mangPhanSo);
                System.out.println("Hiệu các phân số là: " + hieu.tuSo + "/" + hieu.mauSo);
                break;
            case 3:
                mangPhanSo = nhapMangPS(2);
                tich = tinhToan.nhan(mangPhanSo);
                System.out.println("Tích các phân số là: " + tich.tuSo + "/" + tich.mauSo);
                break;
            case 4:
                mangPhanSo = nhapMangPS(2);
                thuong = tinhToan.chia(mangPhanSo);
                System.out.println("Thương các phân số là: " + thuong.tuSo + "/" + thuong.mauSo);
                break;
        }
    }

    public static PhanSo nhapPS(){
        PhanSo phanSo = new PhanSo();
        int ts, ms;
        boolean checkInput = true;
        do {
            try {
                System.out.print("Nhập tử số: ");
                ts = Integer.parseInt(input.nextLine());
                System.out.println("Nhập mẫu số: ");
                ms = Integer.parseInt(input.nextLine());
                if(ms == 0) throw new MyException();
                phanSo = new PhanSo(ts, ms);
                checkInput = true;
            } catch (MyException e) {
                System.out.println(e.getMessage());
                checkInput = false;
            } catch (NumberFormatException e) {
                System.out.println("Nhập sai định dạng, tử số và mẫu số phải là số nguyên");
                checkInput = false;
            } catch (Exception e) {
                e.printStackTrace();
                checkInput = false;
            }
        }while (!checkInput);
        return phanSo;
    }

    public static PhanSo[] nhapMangPS(int n){
        PhanSo mangphanSo[] = new PhanSo[n];
        for (int i = 0; i < n; i++){
            System.out.println("Nhập phân số thứ: " + (i + 1));
            mangphanSo[i] = nhapPS();
            System.out.println("Phân số nhập vào là: " + mangphanSo[i].tuSo + "/" + mangphanSo[i].mauSo);
        }
        return mangphanSo;
    }
}
