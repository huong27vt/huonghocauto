package lesson3;

public class Bai6 {

    public static boolean check(String[] a, String b){
        for(int i =0; i < a.length; i++){
            if(a[i].contains(b)){
                return true;
            }
        }
        return false;
    }
    public static void main(String[] args) {
        String[] fruits = {"Táo", "Ổi", "Lê", "Dứa", "Cam"};
        System.out.println(check(fruits, "Cam"));
    }
}
