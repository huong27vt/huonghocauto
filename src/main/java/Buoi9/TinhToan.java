package Buoi9;

public class TinhToan implements IPhanSo{

//    public TinhToan() {
//    }

    @Override
    public PhanSo cong(PhanSo... args) {
        PhanSo tong = new PhanSo();
        int ts = 1, ms = 1;
        for(int i = 0; i < args.length; i++){
            ts = ts*args[i].mauSo + ms*args[i].tuSo ;
            ms = ms*args[i].mauSo;
        }
        tong.tuSo = ts;
        tong.mauSo = ms;
        return tong;
    }

    @Override
    public PhanSo tru(PhanSo... args) {
        PhanSo hieu = new PhanSo();
        int ts = 1, ms = 1;
        for(int i = 0; i < args.length; i++){
            ts = ts*args[i].mauSo - ms*args[i].tuSo ;
            ms = ms*args[i].mauSo;
        }
        hieu.tuSo = ts;
        hieu.mauSo = ms;
        return hieu;
    }

    @Override
    public PhanSo nhan(PhanSo... args) {
        PhanSo tich = new PhanSo();
        int ts = 1, ms = 1;
        for(int i = 0; i < args.length; i++){
            ts = ts*args[i].tuSo;
            ms = ms*args[i].mauSo;
        }
        tich.tuSo = ts;
        tich.mauSo = ms;
        return tich;
    }

    @Override
    public PhanSo chia(PhanSo... args) {
        PhanSo thuong = new PhanSo();
        int ts = 0, ms = 1;
        for(int i = 0; i < args.length; i++){
            ts = ts*args[i].mauSo;
            ms = ms*args[i].tuSo;
        }
        thuong.tuSo = ts;
        thuong.mauSo = ms;
        return thuong;
    }
}
