package lamda;

import java.util.HashSet;
import java.util.Set;

public class Main {
    @FunctionalInterface
    interface CountString{
        int countString(String chuoi);
    }

    public static void main(String[] args) {
        // Dem so ki tu cua chuoi
        CountString countCharacter = (input) -> {
            System.out.println("Đếm số kí tự của chuỗi: " + input);
            return input.length();
        };

        System.out.println(countCharacter.countString("Java core"));

        // Tinh so tu trong chuoi
        CountString countWord = (input) -> {
            System.out.println("Đếm số từ trong chuỗi: " + input);
            String[] a = input.split(" ");
            return a.length;
        };

        System.out.println(countWord.countString("Java core test"));

        // Dem so tu a or A trong chuoi
        CountString countDetail = (input) -> {
            System.out.println("Đếm số từ lặp lại trong chuỗi ignorecase: " + input);
            String[] a = input.split(" ");
            int dem = 0;
            for(int i = 0; i< a.length; i++){
                if(a[i].equalsIgnoreCase("core")){
                    dem++;
                }
            }
            return dem;
        };

        System.out.println(countDetail.countString("Java core test CORE core Core"));

        // Dem so ki tu duoc su dung trong chuoi
        CountString countWordUsed = (input) -> {
            System.out.println("Đếm số ký tự được sử dụng trong chuỗi " + input + " và in ra: ");
            String a = input.replaceAll("\\s","");
            System.out.println("Chuỗi đã bỏ space: " + a);
            char[] b = a.toCharArray();
            Set<Character> set = new HashSet<>();
            for(int i = 0; i < b.length; i++){
                set.add(b[i]);
            }
            System.out.println("In kí tự: " + set);
            return set.size();
        };

        System.out.println("Số kí tự là: " + countWordUsed.countString("hello hi"));

    }
}
