package generic;

public class Main {
    public static void main(String[] args) {
        Box<Double> box1 = new Box();
        box1.setT(1.5);
        Box<String> box2 = new Box();
        box2.setT("Hello");
        Box<Fruits> box3 = new Box<>(new Fruits("Cam"));

        Fruits qua1 = new Fruits("Táo");
        Box<Fruits> box4 = new Box<>();
        box4.setT(qua1);

        System.out.println(box1);
        System.out.println(box2);
        System.out.println(box4.getT().name);

        Box<String> box5 = new Box<>();
        box5.setT(box3.getT().name);
        System.out.println(box5);

        Box<Box> box6 = new Box<>();
        box6.setT(box3);
        System.out.println(box6.getT().getT().getClass().getSimpleName());
        Fruits f = (Fruits) box6.getT().getT();
        System.out.println(f.name);
    }
}
