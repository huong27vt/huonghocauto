package lesson8;

public class HCN {
    double chieuDai;
    double chieuRong;
    double cv;
    double dt;

    public HCN(){

    }

    public HCN(double chieuDai, double chieuRong){
        this.chieuDai = chieuDai;
        this.chieuRong = chieuRong;
    }

    double tinhChuVi(){
        cv = (chieuDai + chieuRong) * 2;
        return cv;
    }

    double tinhDienTich(){
        dt = chieuDai * chieuRong;
        return dt;
    }

}
