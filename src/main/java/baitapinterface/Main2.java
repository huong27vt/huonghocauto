package baitapinterface;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

public class Main2 {
    public static void main(String[] args) {
        HinhTron hinhTron = new HinhTron(3);
        System.out.println(hinhTron);
        System.out.printf("Diện tích là: %.2f \n", hinhTron.tinhDienTich());
        System.out.printf("Chu vi là: %.2f \n", hinhTron.tinhChuVi());


        HinhTru hinhTru = new HinhTru(2, 6);
        System.out.println(hinhTru);
        System.out.printf("Thể tích là: %.2f \n", hinhTru.tinhTheTich());


        HinhBinhHanh hinhBinhHanh = new HinhBinhHanh(6,3,5);
        System.out.println(hinhBinhHanh);
        System.out.printf("Diện tích là: %.2f \n", hinhBinhHanh.tinhDienTich());
        System.out.printf("Chu vi là: %.2f \n", hinhBinhHanh.tinhChuVi());

        HinhChuNhat hinhChuNhat = new HinhChuNhat(7,5);
        System.out.println(hinhChuNhat);
        System.out.printf("Diện tích là: %.2f \n", hinhChuNhat.tinhDienTich());
        System.out.printf("Chu vi là: %.2f \n", hinhChuNhat.tinhChuVi());
    }

}
