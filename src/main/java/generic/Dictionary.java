package generic;

public class Dictionary <K, V>{
    private K t;
    private V v;

    public Dictionary() {
    }

    public Dictionary(K t, V v) {
        this.t = t;
        this.v = v;
    }

    public K getT() {
        return t;
    }

    public void setT(K t) {
        this.t = t;
    }

    public V getV() {
        return v;
    }

    public void setV(V v) {
        this.v = v;
    }

    @Override
    public String toString() {
        return "Dictionary{" +
                "t=" + t +
                ", v=" + v +
                '}';
    }
}
