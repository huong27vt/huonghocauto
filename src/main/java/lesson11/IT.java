package lesson11;

public class IT extends NhanVien{

    public IT(String maNV, String hoTen, String gioiTinh, int namSinh) {
        super(maNV, hoTen, gioiTinh, namSinh);
    }

    @Override
    public void congViec() {
        System.out.println("Hỗ trợ máy tính");
    }
}
