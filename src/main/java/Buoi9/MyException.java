package Buoi9;

import java.util.InputMismatchException;

public class MyException extends InputMismatchException {
    @Override
    public String getMessage() {
        return "Mẫu số phải khác 0";
    }
}
