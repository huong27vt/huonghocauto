package override_abstract;

public class Main2 {
    public static void main(String[] args) {
        Cat cat = new Cat("con mèo");
        cat.greets();

        Dog dog = new Dog("con chó");
        dog.greets();

        BigDog bigDog1 = new BigDog("bigdog 1");
        bigDog1.greets(dog);
        BigDog bigDog2 = new BigDog("bigdog 2");
        bigDog2.greets(bigDog1);

    }
}
