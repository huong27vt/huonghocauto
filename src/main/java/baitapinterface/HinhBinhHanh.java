package baitapinterface;

public class HinhBinhHanh implements HinhHoc2D{
    protected double canhDay;
    protected double canhKeDay;
    protected double chieuCao;

    public HinhBinhHanh(double canhDay, double canhKeDay, double chieuCao) {
        this.canhDay = canhDay;
        this.canhKeDay = canhKeDay;
        this.chieuCao = chieuCao;
    }

    public HinhBinhHanh(double canhDay, double canhKeDay) {
        this.canhDay = canhDay;
        this.canhKeDay = canhKeDay;
    }

    public double getCanhDay() {
        return canhDay;
    }

    public void setCanhDay(double canhDay) {
        this.canhDay = canhDay;
    }

    public double getCanhKeDay() {
        return canhKeDay;
    }

    public void setCanhKeDay(double canhKeDay) {
        this.canhKeDay = canhKeDay;
    }

    public double getChieuCao() {
        return chieuCao;
    }

    public void setChieuCao(double chieuCao) {
        this.chieuCao = chieuCao;
    }

    @Override
    public double tinhChuVi() {
        return 2*(canhDay + canhKeDay);
    }

    @Override
    public double tinhDienTich() {
        return canhDay*chieuCao;
    }

    @Override
    public String toString() {
        return "HinhBinhHanh{" +
                "canhDay=" + canhDay +
                ", canhKeDay=" + canhKeDay +
                ", chieuCao=" + chieuCao +
                '}';
    }
}
