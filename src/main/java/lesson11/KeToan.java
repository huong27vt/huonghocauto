package lesson11;

public class KeToan extends NhanVien{

    public KeToan(String maNV, String hoTen, String gioiTinh, int namSinh) {
        super(maNV, hoTen, gioiTinh, namSinh);
    }

    @Override
    public void congViec() {
        System.out.println("TÍnh tiền lương");
    }
}
