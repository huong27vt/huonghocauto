package lesson10;

import java.util.Scanner;

public class TaiLieu {
    private String code;
    private String nsx;
    private int noPublish = 50;


    public TaiLieu(String code, String nsx, int noPublish) {
        this.code = code;
        this.nsx = nsx;
        this.noPublish = noPublish;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNsx() {
        return nsx;
    }

    public void setNsx(String nsx) {
        this.nsx = nsx;
    }

    public int getNoPublish() {
        return noPublish;
    }

    public void setNoPublish(int noPublish) {
        this.noPublish = noPublish;
    }



    public void input(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Nhập mã tài liệu: ");
        code = scan.nextLine();
        System.out.println("Nhập tên nhà xuất bản: ");
        nsx = scan.nextLine();
        System.out.println("Nhập số bản phát hành: ");
        noPublish = scan.nextInt();
    }

    public void output(){
        System.out.printf("%-20s %-20s %-20d", code, nsx, noPublish);
    }
}
